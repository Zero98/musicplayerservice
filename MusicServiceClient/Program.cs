﻿using System;

namespace MusicServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("**** Ask the Music service ****");

            ClientMenu clientMenu = new ClientMenu();
            clientMenu.RunClient();

            Console.ReadKey();
        }
    }
}
