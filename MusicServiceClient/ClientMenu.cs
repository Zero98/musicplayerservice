﻿using System;
using MusicServiceClient.Commands;
using System.Threading;
using ServiceAPI.Model;

namespace MusicServiceClient
{
    class ClientMenu
    {
        private ICommandBehavior commandBehavior;

        public void RunClient()
        {
            CommandList commandList = new CommandList();

            commandList.CallCommand("help");

            do
            {
                Console.WriteLine("Input the command:");

                try
                {
                    string command = Console.ReadLine().ToLower();

                    if (commandList.СontainCommand(command))
                    {
                        commandBehavior = commandList.CallCommand(command);
                    }
                    else
                    {
                        Console.WriteLine("Incorrect command!");
                    }
                }
                catch
                {
                    Console.WriteLine("The server terminated the connection");
                }
                finally
                {
                    Console.WriteLine("----");
                }
                
                
            } while (!(commandBehavior is ExitCommandBehavior));
        }

        public static void WriteLine(string requestMsg)
        {
            Console.WriteLine(requestMsg);
        }

        public static string GetField(string requestMsg)
        {
            Console.WriteLine(requestMsg);

            return Console.ReadLine();
        }
        
        public static object Wait(Func<ServiceRequest, object, object> service,
            ServiceRequest serviceRequest,
            object data = null)
        {
            IAsyncResult asyncResult = service.BeginInvoke(serviceRequest, data, null, null);

            WriteLoad(asyncResult);

            return service.EndInvoke(asyncResult);
        }

        public static void ShowResponse(object responseObj)
        {
            var response = responseObj as ServiceAPI.PlayerServiceRef.Response;

            Console.WriteLine(response.Status);

            if(response.AdditionalInformation != null)
                for(int i = 0; i < response.AdditionalInformation.Length; i++)
                    Console.WriteLine($"{i}. {response.AdditionalInformation[i]}");
        }

        private static void WriteLoad(IAsyncResult result)
        {
            while (!result.IsCompleted)
            {
                for(int i = 0; i < 3; i++)
                {
                    Console.Write('.');
                    Thread.Sleep(100);
                }
                Console.Clear();
            }
            Console.WriteLine();
        }
    }
}
