﻿using ServiceAPI.Model;

namespace MusicServiceClient.Commands
{
    class SearcherCommandBehavior : ICommandBehavior
    {
        public void ExecuteAction()
        {
            string query = ClientMenu.GetField("Input the part of the name to find composition in the catalog:");

            var responseObj = ClientMenu.Wait(
                Service.RequestToService, ServiceRequest.Search,
                query
                );

            ClientMenu.ShowResponse(responseObj);
        }
    }
}
