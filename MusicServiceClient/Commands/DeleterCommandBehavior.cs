﻿using ServiceAPI.Model;

namespace MusicServiceClient.Commands
{
    class DeleterCommandBehavior : ICommandBehavior
    {
        public void ExecuteAction()
        {
            string query = ClientMenu.GetField("Input the full name of the track to remove:");
            var responseObj = ClientMenu.Wait(
                Service.RequestToService, ServiceRequest.Delete, query
                );

            ClientMenu.ShowResponse(responseObj);
        }
    }
}
