﻿using ServiceAPI.Model;

namespace MusicServiceClient.Commands
{
    class AdderCommandBehavior : ICommandBehavior
    {
        public void ExecuteAction()
        {
            var song = new Song(
                ClientMenu.GetField("Input author's name:"),
                ClientMenu.GetField("Input the composition's name:"),
                ClientMenu.GetField("Input the reference:"));

            if (song.IsCorrectSong())
            {
                var responseObj = ClientMenu.Wait(
                    Service.RequestToService, ServiceRequest.Add, song
                    );

                ClientMenu.ShowResponse(responseObj);
            }
            else
            {
                ClientMenu.WriteLine("Incorrect data");
            }
        }
    }
}
    