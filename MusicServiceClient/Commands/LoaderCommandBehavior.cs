﻿using ServiceAPI.Model;
namespace MusicServiceClient.Commands
{
    class LoaderCommandBehavior : ICommandBehavior
    {
        public void ExecuteAction()
        {
            ClientMenu.WriteLine("Selecte file");

            var responseObj = ClientMenu.Wait(
                Service.RequestToService, ServiceRequest.GetLoadableList
                );

            var responseString = responseObj as string;

            string[] listOfFiles = responseString.Split('\n');

            ShowFiles(listOfFiles);

            string selectedFile = ClientMenu.GetField("Enter the number of the selected directory");

            int selectedFileInInt;
            if (int.TryParse(selectedFile, out selectedFileInInt)
                      && selectedFileInInt < listOfFiles.Length
                      && selectedFileInInt >= 0)
            {
                responseObj = ClientMenu.Wait(
                Service.RequestToService, ServiceRequest.Load,
                listOfFiles[selectedFileInInt]
                );

                ClientMenu.ShowResponse(responseObj);
            }
            else
            {
                ClientMenu.WriteLine("You select incorrect file!");
            }
        }

        private void ShowFiles(string[] listOfFiles)
        {
            for (int i = 0; i < listOfFiles.Length-1; i++)
            {
                ClientMenu.WriteLine($"{i}. {listOfFiles[i]}");
            }
        }
    }
}