﻿using ServiceAPI.Model;

namespace MusicServiceClient.Commands
{
    class SaverCommandBehavior : ICommandBehavior
    {
        public void ExecuteAction()
        {
            var fileName = ClientMenu.GetField("Enter the name of the file");

            var responseObj = ClientMenu.Wait(
                Service.RequestToService, ServiceRequest.Save,
                fileName
                );

            ClientMenu.ShowResponse(responseObj);
        }
    }
}
