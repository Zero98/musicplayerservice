﻿namespace MusicServiceClient.Commands
{
    interface ICommandBehavior
    {
        void ExecuteAction();
    }
}
