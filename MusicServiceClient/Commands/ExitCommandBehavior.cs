﻿namespace MusicServiceClient.Commands
{
    class ExitCommandBehavior : ICommandBehavior
    {
        public void ExecuteAction()
        {
            ClientMenu.WriteLine("Bye!");
        }
    }
}
