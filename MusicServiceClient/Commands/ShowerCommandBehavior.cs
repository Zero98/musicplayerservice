﻿using ServiceAPI.Model;

namespace MusicServiceClient.Commands
{
    class ShowerCommandBehavior : ICommandBehavior
    {
        public void ExecuteAction()
        {
            ClientMenu.WriteLine("All compositions in catalog");

             var responseObj = ClientMenu.Wait(
                Service.RequestToService, ServiceRequest.ShowAll
                );

            ClientMenu.ShowResponse(responseObj);
        }
    }
}
