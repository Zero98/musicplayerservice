﻿using System;
using ServiceAPI.PlayerServiceRef;
using ServiceAPI.Model;
using ServiceAPI;
using System.Threading;

namespace Console1
{
    class Program
    {
        public static string Wait(Func<ServiceRequest, object, string> service,
            ServiceRequest serviceRequest,
            object data = null)
        {
            IAsyncResult asyncResult = service.BeginInvoke(serviceRequest, data, null, null);
            WriteLoad(asyncResult);

            return service.EndInvoke(asyncResult);
        }

        private static void WriteLoad(IAsyncResult result)
        {
            while (!result.IsCompleted)
            {
                for (int i = 0; i < 3; i++)
                {
                    Console.Write('.');
                    Thread.Sleep(100);
                }
                Console.Clear();
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Input the part of the name to find composition in the catalog:");
            string query = Console.ReadLine();

            string response = Wait(
                Service.RequestToService, ServiceRequest.Search,
                query
                );
            Console.WriteLine(response);
            Console.ReadKey();
        }
    }
}
