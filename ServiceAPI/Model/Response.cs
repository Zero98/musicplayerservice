﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ServiceAPI.Model
{
    [DataContract]
    public class Response
    {
        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public List<string> AdditionalInformation { get; private set; }

        public Response() { }

        public Response(string status) : this()
        {
            Status = status;
        }

        public Response(string status, List<string> additionalInformation) : this(status)
        {
            AdditionalInformation = additionalInformation;
        }

        public void AddAdditionalInformation(string responseLine)
        {
            AdditionalInformation.Add(responseLine);
        }
    }
}
