﻿using MusicClient = ServiceAPI.PlayerServiceRef.MusicServiceClient;
using Response = ServiceAPI.PlayerServiceRef.Response;
using TransferSong = ServiceAPI.PlayerServiceRef.Song;
using BaseTextWithID = ServiceAPI.Model.TextWithID;
using System;

namespace ServiceAPI.Model
{
    public enum ServiceRequest
    {
        Add = 0,
        Delete = 1,
        Search = 2,
        ShowAll = 3,
        GetLoadableList = 4,
        Load = 5,
        Save = 6
    }

    public class Service
    {
        public static object RequestToService(ServiceRequest serviceRequest, object transferData = null)
        {
            var receivedData = new object();
            using (MusicClient client = new MusicClient())
            {
                receivedData = MakeRequest(client, transferData, serviceRequest);
            }
            return receivedData;
        }

        private static object MakeRequest(MusicClient client, object transferData, ServiceRequest serviceRequest)
        {
            int id = User.Instance().ID;
            switch (serviceRequest)
            {
                case ServiceRequest.Add:
                    {
                        Song song = transferData as Song;
                        if (song is Song)
                        {
                            song.Transfer.UserID = id;
                            return client.Add(song.Transfer);
                        }
                        break;
                    }
                case ServiceRequest.Delete:
                    {
                        string removableSong = transferData as string;
                        if (removableSong is string)
                        {
                            return client.Delete(new BaseTextWithID(removableSong).Transfer);
                        }
                        break;
                    }
                case ServiceRequest.Search:
                    {
                        string requestedSong = transferData as string;
                        if (requestedSong is string)
                        {
                            return client.Search(new BaseTextWithID(requestedSong).Transfer);
                        }
                        break;
                    }
                case ServiceRequest.ShowAll:
                    {
                        return client.ShowAll(id);
                    }
                case ServiceRequest.GetLoadableList:
                    {
                        return client.GetLoadableList(id);
                    }
                case ServiceRequest.Load:
                    {
                        string loadingPlaylist = transferData as string;
                        if (loadingPlaylist is string)
                        {
                            return client.Load(new BaseTextWithID(loadingPlaylist).Transfer);
                        }
                        break;
                    }
                case ServiceRequest.Save:
                    {
                        string header = transferData as string;
                        if (header is string)
                        {
                            return client.Save(new BaseTextWithID(header).Transfer);
                        }
                        break;
                    }
            }

            return null;
        }

        public static object RequestToService(object search, object text)
        {
            throw new NotImplementedException();
        }
    }
}
