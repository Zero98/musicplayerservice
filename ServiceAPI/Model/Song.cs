﻿namespace ServiceAPI.Model
{
    public class Song
    {
        public string Author
        {
            get { return Transfer.Author; }
            set { Transfer.Author = value; }
        }

        public string Name
        {
            get { return Transfer.Name; }
            set { Transfer.Name = value; }
        }

        public string Reference
        {
            get { return Transfer.Reference; }
            set { Transfer.Reference = value; }
        }

        public int Length { get { return Transfer.Length; } }

        public PlayerServiceRef.Song Transfer { get; }

        public Song(string author, string name, string reference, int length = -1)
        {
            Transfer = new PlayerServiceRef.Song();

            Transfer.Author = author;
            Transfer.Name = name;
            Transfer.Reference = reference;
            Transfer.Length = -1;
            Transfer.UserID = User.Instance().ID;
        }

        public Song()
        {
            Transfer = new PlayerServiceRef.Song();
        }

        public override string ToString()
        {
            return $"{Author} - {Name}";
        }

        public bool IsCorrectSong()
        {
            return Name != default(string);
        }
    }
}
