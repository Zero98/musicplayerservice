﻿namespace ServiceAPI.Model
{
    public class TextWithID
    {
        public PlayerServiceRef.TextWithID Transfer { get; }

        public string Text { get { return Transfer.Text; } }

        public int UserID { get { return Transfer.UserID; } }

        public TextWithID(string text)
        {
            Transfer = new PlayerServiceRef.TextWithID();

            Transfer.Text = text;
            Transfer.UserID = User.Instance().ID;
        }

        public TextWithID() { }
    }
}
