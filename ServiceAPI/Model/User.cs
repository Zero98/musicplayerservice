﻿using System;

namespace ServiceAPI.Model
{
    public class User
    {
        private static User instance;

        private User()
        {
            Random random = new Random();
            ID = random.Next(0, 1000);
        }

        public static User Instance()
        {
            if (instance == null)
                instance = new User();
            return instance;
        }

        public int ID { get; private set; }
    }
}
