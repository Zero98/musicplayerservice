﻿using System.Windows;
using MusicPlayerGUI.ViewModel;

namespace MusicPlayerGUI.View
{
    /// <summary>
    /// Interaction logic for AddSongWindow.xaml
    /// </summary>
    public partial class AddSongWindow : Window
    {
        public AddSongWindow()
        {
            InitializeComponent();
            DataContext = new SongViewModel(); 
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            var song = (SongViewModel)DataContext;
            if (((SongViewModel)DataContext).Verify())
            {
                song.AddToPlayList();
                DialogResult = true;
            }
            else
            {
                VerifyTextBox.Visibility = Visibility.Visible;           
            }
        }
    }
}
