﻿using System.Windows;
using MusicPlayerGUI.ViewModel;

namespace MusicPlayerGUI.View
{
    /// <summary>
    /// Interaction logic for DelSongWindow.xaml
    /// </summary>
    public partial class DelSongWindow : Window
    {
        public DelSongWindow(string deletedContent)
        {
            InitializeComponent();
            DataContext = deletedContent;
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            var deletedSong = new SongViewModel();
            deletedSong.DeleteSong((string)DataContext);
            DialogResult = true;
        }

        private void No_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
