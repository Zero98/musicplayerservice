﻿using MusicPlayerGUI.ViewModel;
using System.Windows;

namespace MusicPlayerGUI.View
{
    /// <summary>
    /// Interaction logic for SaveWindow.xaml
    /// </summary>
    public partial class SaveWindow : Window
    {
        public SaveWindow()
        {
            InitializeComponent();
            DataContext = new PlaylistIOViewModel();
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            var playlistIO = (PlaylistIOViewModel)DataContext;
            playlistIO.SavePlayList();
            DialogResult = true;
        }

        private void No_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
