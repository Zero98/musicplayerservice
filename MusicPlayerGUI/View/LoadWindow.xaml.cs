﻿using System.Windows;
using System.Windows.Controls;
using MusicPlayerGUI.ViewModel;

namespace MusicPlayerGUI.View
{
    /// <summary>
    /// Interaction logic for LoadWindow.xaml
    /// </summary>
    public partial class LoadWindow : Window
    {
        private PlaylistIOViewModel playlistIO;

        public LoadWindow()
        {
            InitializeComponent();
            playlistIO = new PlaylistIOViewModel();
            ShowFiles();            
        }

        private void ShowFiles()
        {
            for(int i = 0; i < playlistIO.Files.Length - 1; i++)
            {
                FileListView.Items.Add(playlistIO.Files[i]);
            }
        }

        private void Load_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            playlistIO.Filename = (string)((ListViewItem)sender).Content;
            playlistIO.LoadPlayList();
            DialogResult = true;
        }
    }
}
