﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MusicPlayerGUI.ViewModel;
using MusicPlayerGUI.View;

namespace MusicPlayerGUI
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new PlaylistViewModel();
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var text = ((TextBox)sender).Text;
            if (text != "")
            {
                var foundEntriesText = ((PlaylistViewModel)DataContext).SearchSongs(text);

                UpdtateSearchListView(foundEntriesText);

                SearchListView.Visibility = Visibility.Visible;
            }
            else
            {
                SearchListView.Visibility = Visibility.Hidden;
            }
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            var addSongWindow = new AddSongWindow();
            addSongWindow.ShowDialog();
            UpdateUserListView();
        }

        private void Del_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var content = (string)((ListViewItem)sender).Content;
            var delSongWindow = new DelSongWindow(content);
            delSongWindow.ShowDialog();
            UpdateUserListView();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            var saveWindow = new SaveWindow();
            saveWindow.ShowDialog();
            UpdateUserListView();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            var loadWindow = new LoadWindow();
            loadWindow.ShowDialog();
            UpdateUserListView();
        }

        private void UpdateUserListView()
        {
            ((PlaylistViewModel)DataContext).UpdateSongList();
            UsersPlaylist.Items.Clear();

            foreach(var song in ((PlaylistViewModel)DataContext).Songs)
            {
                UsersPlaylist.Items.Add(song);
            }
        }

        private void UpdtateSearchListView(string[] foundEntriesText)
        {
            SearchListView.Items.Clear();

            foreach (var line in foundEntriesText)
            {
                SearchListView.Items.Add(line);
            }
        }
    }
}
