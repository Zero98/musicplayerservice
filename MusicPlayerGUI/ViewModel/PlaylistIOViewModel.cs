﻿using ServiceAPI.Model;
using Response = ServiceAPI.PlayerServiceRef.Response;

namespace MusicPlayerGUI.ViewModel
{
    class PlaylistIOViewModel
    {
        public string Filename { get; set; }
        
        public string[] Files
        {
            get
            {
                var response = (string)Service.RequestToService(ServiceRequest.GetLoadableList);
                return response.Split('\n');
            }
        }

        public PlaylistIOViewModel()
        {
            Filename = "Какой-то_файл.pls";
        }

        public void SavePlayList() 
        {
            var response = (Response)Service.RequestToService(ServiceRequest.Save, Filename);
        }

        public void LoadPlayList()
        {
            var response = (Response)Service.RequestToService(ServiceRequest.Load, Filename);
        }
    }
}