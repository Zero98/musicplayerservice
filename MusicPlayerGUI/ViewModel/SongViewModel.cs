﻿using ServiceAPI.Model;
using Response = ServiceAPI.PlayerServiceRef.Response;

namespace MusicPlayerGUI.ViewModel
{
    public class SongViewModel
    {
        private Song newSong;

        public string Author
        {
            get { return newSong.Author; }
            set { newSong.Author = value; }
        }

        public string SongName
        {
            get { return newSong.Name; }
            set { newSong.Name = value; }
        }

        public string SongRef
        {
            get { return newSong.Reference; }
            set { newSong.Reference = value; }
        }

        public SongViewModel() => newSong = new Song();

        public void AddToPlayList()
        {
            var response = Service.RequestToService(ServiceRequest.Add, newSong) as Response;
        }

        public void DeleteSong(string authorAndName)
        {
            var response = (Response)Service.RequestToService(ServiceRequest.Delete, authorAndName);
        }

        public bool Verify()
        {
            return (!string.IsNullOrWhiteSpace(Author) &&
                    !string.IsNullOrWhiteSpace(SongName) && 
                    !string.IsNullOrWhiteSpace(SongRef)) ? true : false;
        }
    }
}
    