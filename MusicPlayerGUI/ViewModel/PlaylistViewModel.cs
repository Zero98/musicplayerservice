﻿using ServiceAPI.Model;
using Response = ServiceAPI.PlayerServiceRef.Response;
using System.Collections.Generic;

namespace MusicPlayerGUI.ViewModel
{
    public class PlaylistViewModel
    {
        public List<string> Songs { get; set; }
        public string CurrentStatus { get; private set; }
            
        public PlaylistViewModel()
        {   
            CurrentStatus = null;
            Songs = new List<string>();
            UpdateSongList();
        }

        public void UpdateSongList()
        {  
            var response = (Response)Service.RequestToService(ServiceRequest.ShowAll);
            if (response.AdditionalInformation != null) Songs.Clear();

            foreach (var song in response.AdditionalInformation)
            {
                Songs.Add(song);
            }

            CurrentStatus = response.Status;
        }

        public string[] SearchSongs(string searchString)
        {
            var responseObj = Service.RequestToService(ServiceRequest.Search, searchString);
            var response = (Response)responseObj;

            CurrentStatus = response.Status;

            return response.AdditionalInformation;
        }
    }
}
