﻿using MusicPlayerServiceLib.UsersState;
using MusicPlayerServiceLib.Model;
using System.Text;
using System;

namespace MusicPlayerServiceLib
{
    class Host
    {
        const string SUCCESSFULLY = "SUCCESSFULLY";
        const string UNSUCCESSFUL = "UNSUCCESSFUL";

        private PlaylistFile playlistFile;
        private ConnectorDB connectorDB;
        private static Host instance;

        private Host()
        {
            connectorDB = new ConnectorDB();
            playlistFile = new PlaylistFile();
        }

        public static Host GetInstance()
        {
            if (instance == null)
                instance = new Host();
            return instance;
        }

        private Response PrepareResponse(Songs[] songs)
        {
            var response = new Response();
            response.Status = SUCCESSFULLY;
            for (int count = 0; count < songs.Length; count++)
            {
                var responseLine = new StringBuilder();
                //
                responseLine.Append(songs[count].Author);
                responseLine.Append(" - ");
                responseLine.Append(songs[count].Name);
                response.AddAdditionalInformation(responseLine.ToString());
            }

            return response;
        }

        public Response AddSong(Song newSong)
        {
            try
            {
                connectorDB.AddNewRecord(newSong);
                return new  Response(SUCCESSFULLY);
            }
            catch
            {
                return new Response(UNSUCCESSFUL);
            }
        }

        public Response GetPlaylistByUserID(int userID)
        {
            try
            {
                var usersPlaylist = connectorDB.GetUsersPlaylist(userID);
                return PrepareResponse(usersPlaylist);
            }
            catch
            {
                return new Response(UNSUCCESSFUL);
            }
        }

        public Response DeleteByUserRequest(TextWithID textWithID)
        {
            try
            {
                connectorDB.DeleteByRequest(textWithID.UserID, textWithID.Text);
                return new Response(SUCCESSFULLY);
            }
            catch (ArgumentNullException anx)
            {
                return new Response("Такой песни нет в вашем плейлисте");
            }
            catch
            {
                return new Response(UNSUCCESSFUL);
            }
        }

        public Response SearchByUserRequest(TextWithID textWithID)
        {
            try
            {
                var songs = connectorDB.Search(textWithID.UserID, textWithID.Text);
                return PrepareResponse(songs);
            }
            catch
            {
                return new Response(UNSUCCESSFUL);
            }
        }

        public string GetAvailablePlaylists()
        {
            try
            {
                return playlistFile.GetListFiles();
            }
            catch
            {
                return UNSUCCESSFUL;
            }
        }

        public Response LoadPlaylist(TextWithID textWithID)
        {
            try
            {
                playlistFile.LoadPlayList(textWithID.UserID, textWithID.Text);
                return new Response(SUCCESSFULLY);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new Response(UNSUCCESSFUL);
            }
        }

        public Response SavePlayList(TextWithID textWithID)
        {
            try
            {
                playlistFile.Save(textWithID.UserID, textWithID.Text);

                return new Response(SUCCESSFULLY);
            }
            catch
            {
                return new Response(UNSUCCESSFUL);
            }
        }
    }
}
