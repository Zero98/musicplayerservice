﻿using System;
using System.Collections.Generic;
using MusicPlayerServiceLib.Model;

namespace MusicPlayerServiceLib
{
    public class MusicService : IMusicService
    {
        public List<int> vs = new List<int>();

        public void ShowConnectionInfo(int id)
        {
            Console.WriteLine($"User {id} has connected to the service");
        }

        public Response Add(Song song)
        {
            var host = Host.GetInstance();
            return host.AddSong(song);
        }

        public Response Delete(TextWithID textWithID)
        {
            var host = Host.GetInstance();
            return host.DeleteByUserRequest(textWithID);
        }

        public string GetLoadableList(int id)
        {
            var host = Host.GetInstance();
            return host.GetAvailablePlaylists();
        }

        public Response Load(TextWithID textWithID)
        {
            var host = Host.GetInstance();
            return host.LoadPlaylist(textWithID);
        }

        public Response Save(TextWithID textWithID)
        {
            var host = Host.GetInstance();
            return host.SavePlayList(textWithID);
        }

        public Response Search(TextWithID textWithID)
        {
            var host = Host.GetInstance();
            return host.SearchByUserRequest(textWithID);
        }

        public Response ShowAll(int id)
        {
            var host = Host.GetInstance();
            return host.GetPlaylistByUserID(id);
        }
    }
}