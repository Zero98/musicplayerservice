﻿using System.Runtime.Serialization;

namespace MusicPlayerServiceLib.Model
{
    [DataContract]
    public class TextWithID
    {
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public int UserID { get; set; }

        public TextWithID(string text, int userID)
        {
            Text = text;
            UserID = userID;
        }

        public TextWithID() { }
    }
}
