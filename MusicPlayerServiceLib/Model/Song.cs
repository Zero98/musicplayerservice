﻿using System.Runtime.Serialization;

namespace MusicPlayerServiceLib.Model
{
    [DataContract]
    public class Song
    {
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Reference { get; set; }
        [DataMember]
        public int Length { get; set; }
        [DataMember]
        public int UserID { get; set; }

        public Song(string author, string name, string reference, int length = -1)
        {
            Author = author;
            Name = name;
            Reference = reference;
            Length = -1;
        }

        public Song() { }

        public override string ToString()
        {
            return $"{Author} - {Name}";
        }

        public bool IsCorrectSong()
        {
            return Name != default(string);
        }
    }
}
