namespace MusicPlayerServiceLib.UsersState
{
    using System.Linq;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Data.Entity;

    public partial class Users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Users()
        {
            Songs = new HashSet<Songs>();
        }

        public int Id { get; set; }

        public int User_ID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Songs> Songs { get; set; }
    }

    public static class UserExtention
    {
        public static bool TryContains(this DbSet<Users> users, Users user)
        {
            try
            {
                return users.Contains(user);
            }
            catch
            {
                return false;
            }
        }
    }
}
