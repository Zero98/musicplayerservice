﻿using System.Linq;
using System.Text;
using MusicPlayerServiceLib.Model;

namespace MusicPlayerServiceLib.UsersState
{
    class ConnectorDB
    {
        public Songs[] GetUsersPlaylist(int userID)
        {
            using (var context = new PlaylistEntity())
            {
                return context.Songs.Where(p => (p.Users.Any(c => c.User_ID == userID))).ToArray();
            }
        }

        public void DeleteByRequest(int userID, string text)
        {
            using (var context = new PlaylistEntity())
            {
                var removableSong = context
                    .Users
                    .Where(c => c.User_ID == userID &&
                    c.Songs.
                    Contains(
                        c.Songs
                        .Where(p => (p.Author + " - " + p.Name) == text
                        )
                    .FirstOrDefault()))
                    .FirstOrDefault();

                context.Users.Remove(removableSong);
                context.SaveChanges();
            }
        }

        private void CollectRemovableString(Songs song, int userID)
        {
            try
            {
                StringBuilder removableSong = new StringBuilder();
                removableSong.Append(song.Author);
                removableSong.Append(" - ");
                removableSong.Append(song.Name);
                DeleteByRequest(userID, removableSong.ToString());
            }
            catch { }
        }

        public void DeleteSongsFromPlaylist(int userID)
        {
            var songs = GetUsersPlaylist(userID);
            foreach (var song in songs)
            {
                CollectRemovableString(song, userID);
            }
        }

        public void AddNewRecord(Song newSong)
        {
            using (var context = new PlaylistEntity())
            {
                var user = new Users {  User_ID = newSong.UserID };
                if (!context.Users.TryContains(user))
                {
                    context.Users.Add(user);
                    context.SaveChanges();
                }

                var songs = new Songs()
                {
                    Author = newSong.Author,
                    Length = newSong.Length,
                    Name = newSong.Name,
                    Reference = newSong.Reference
                };

                songs.Users.Add(user);
                context.Songs.Add(songs);
                context.SaveChanges();
            }
        }

        public void AddNewPlaylist(Song[] newPlaylist, int userID)
        {
            foreach (var song in newPlaylist)
            {
                song.UserID = userID;
                AddNewRecord(song);
            }
        }

        public Songs[] Search(int userID, string request)
        {
            using (var context = new PlaylistEntity())
            {
                var requested = context.Songs.Where(n => (n.Author + " - " + n.Name).Contains(request));

                return requested.ToArray();
            }
        }
    }
}
