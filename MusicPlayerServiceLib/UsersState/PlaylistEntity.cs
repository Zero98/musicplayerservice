namespace MusicPlayerServiceLib.UsersState
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PlaylistEntity : DbContext
    {
        public PlaylistEntity()
            : base("PlaylistEntity")
        {
        }

        public virtual DbSet<Songs> Songs { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<PlaylistEntity>(new DropCreateDatabaseIfModelChanges<PlaylistEntity>());

            modelBuilder.Entity<Songs>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Songs)
                .Map(m => m.ToTable("UsersPlaylist").MapLeftKey("Song_Id").MapRightKey("User_Id"));
        }
    }
}
