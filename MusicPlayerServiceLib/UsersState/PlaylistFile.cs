﻿using System.Text;
using System.IO;
using MusicPlayerServiceLib.Model;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;
using System.Linq;

namespace MusicPlayerServiceLib.UsersState
{
    struct SongField
    {
        public string SongReference;
        public string SongAuthor;
        public string SongName;
        public string SongLength;
        public int? SongID;
        public bool Empty;
    }

    public class PlaylistFile
    {
        public string GetListFiles()
        {
            string[] filesname = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.pls");
            return JoinFilename(filesname);
        }

        public void Save(int UserID, string filename)
        {
            var connectorDB = new ConnectorDB();
            var playList = connectorDB.GetUsersPlaylist(UserID);

            SaveToPls(filename, playList);
        }

        public void LoadPlayList(int UserID, string filename)
        {
            var connectorDB = new ConnectorDB();
            connectorDB.DeleteSongsFromPlaylist(UserID);

            var playlist = LoadPlsFile(filename);
            connectorDB.AddNewPlaylist(playlist, UserID);
        }

        private string JoinFilename(string[] filesname)
        {
            StringBuilder joiningFilenameList = new StringBuilder();
            for (int i = 0; i < filesname.Length; i++)
            {
                joiningFilenameList.Append(filesname[i]);
                joiningFilenameList.Append('\n');
            }

            return joiningFilenameList.ToString();
        }

        private void SaveToPls(string fileName, Songs[] songs)
        {
            FileStream file = new FileStream(fileName, FileMode.Create);

            using (StreamWriter writer = new StreamWriter(file))
            {
                writer.WriteLine("[playlist]\nNumberOfEntries={0}", songs.Length);

                for (int i = 0; i < songs.Length; i++)
                {
                    writer.WriteLine($"File{i}={songs[i].Reference}");
                    writer.WriteLine($"Title{i}={songs[i].Author} - {songs[i].Name}");
                    writer.WriteLine($"Length{i}={songs[i].Length}\n");
                }
            }
        }

        private Song[] LoadPlsFile(string filename)
        {
            List<Song> songs = new List<Song>();

            using (StreamReader reader = new StreamReader(filename))
            {
                int countOfEntries;

                if (reader.ReadLine() == "[playlist]" &&
                        Int32.TryParse(
                        reader.ReadLine().Replace("NumberOfEntries=", ""),
                        out countOfEntries))
                {
                    SongField helpSongField = new SongField() { Empty = true };
                    for (int itter = 0; itter < countOfEntries; itter++)
                    {
                        SongField song = helpSongField;

                        song.SongID = itter;

                        while (!IsNewSong(reader.ReadLine(), ref song, ref helpSongField)) ;

                        AddNewSong(song, songs);
                    }
                }
                else
                {
                    throw new Exception();
                }
                    
            }
            return songs.ToArray();
        }

        private void AddNewSong(SongField song, List<Song> songs)
        {
            if (!song.Empty)
            {
                songs.Add(new Song(
                    song.SongAuthor,
                    song.SongName,
                    song.SongReference,
                    song.SongLength != null ? int.Parse(song.SongLength) : -1)
                    );
            }
        }

        private bool IsNewSong(string line, ref SongField song, ref SongField helpSongFiled)
        {
            if (!String.IsNullOrEmpty(line))
            {
                if (line.Count(n => n == '=') == 1)
                {
                    var lines = line.Split('=');

                    var songID = int.Parse(
                        new string(
                                lines[0].ToCharArray().Where(
                                n => char.IsDigit(n)).ToArray()));
                    var songField = new string(
                        lines[0].ToCharArray().Where(
                            n => !char.IsDigit(n)).ToArray());

                    return song.SongID == songID || song.SongID == null
                        ? FillField(
                            songField,
                            lines[1], ref song)
                        : FillField(
                            songField,
                            lines[1],
                            ref helpSongFiled, true);
                }
                song.Empty = true;
            }
            return true;
        }

        private bool FillField(string stringFild,
            string valueField,
            ref SongField song,
            bool isNew = false)
        {
            switch (stringFild)
            {
                case "File":
                    {
                        song.SongReference = valueField;
                        break;
                    }
                case "Title":
                    {
                        Regex regex = new Regex(@"\s[-]\s");

                        var splittedValueField = regex.Split(valueField);
                        song.SongAuthor = splittedValueField[0];
                        song.SongName = splittedValueField[1];

                        break;
                    }
                case "Length":
                    {
                        song.SongLength = valueField;
                        break;
                    }
                default:
                    {
                        throw new Exception("Wrong field in the file!");
                    }
            }
            song.Empty = false;
            return isNew;
        }
    }
}