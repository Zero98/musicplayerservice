﻿using System;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.ServiceModel;
using MusicPlayerServiceLib;

namespace MusicPlayerServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("**** Console Based WCF Host ****");
            using(WebServiceHost serviceHost = new WebServiceHost(typeof(MusicService), new Uri("http://localhost:8080/MusicService")))
            {
                ServiceEndpoint ep = serviceHost.AddServiceEndpoint(typeof(IMusicService), new WebHttpBinding(), "");
                
                serviceHost.Open();
                Console.WriteLine("The service is ready!");
                DisplayInfo(serviceHost);
                Console.WriteLine("Press the Enter key to terminate service.");
                Console.ReadKey();
            }
        }

        static void DisplayInfo(ServiceHost host)
        {
            Console.WriteLine("\n**** Host Info ****");
            foreach(ServiceEndpoint se in host.Description.Endpoints)
            {
                Console.WriteLine($"Address: {se.Address}");
                Console.WriteLine($"Binding: {se.Binding.Name}");
                Console.WriteLine($"Contract: {se.Contract.Name}");
                Console.WriteLine();
            }
        }
    }
}
